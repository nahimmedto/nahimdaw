<?php
	session_start();
	  if(isset($_SESSION['usuario'])){
	    if($_SESSION['usuario']['nombreRol']!="organizador"){
	      header('Location: pagina-principal.php');
	    }
	  }else{
	      header('Location: InterfazLogin.php');
    }
    require_once("../Models/modelEditarPremios.php");

  //recuperar el caso que se va a editar
  $caso_id = htmlspecialchars($_GET["caso_id"]);
  
  include("header.html");  

  //$lugar_id = recuperar_lugar($caso_id);
  $editar = 1;
  $array= recuperar_info($caso_id);
  include("editarPremios.html");
  
  
  include("footer.html"); 
?>