<?php
 function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","jurassic");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }

  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

function consultar_registros(){
      $conexion_bd = conectar_bd();

      $resultado = "<table><thead>
            <tr>
                <th>Incidente</th>
                <th>Lugar</th>
                <th>Fecha</th>
            </tr>
        </thead>";

      $consulta = 'SELECT t.nombre as Tipo, l.nombre as lugar, fechaCreacion
		FROM lugar l, tipo t, incidente i
		WHERE t.idTipo = i.idTipo 
		AND l.idLugar = i.idLugar
		ORDER BY fechaCreacion DESC';
      $resultados = $conexion_bd->query($consulta);
      while($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)){
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['Tipo']."</td>";
        $resultado .= "<td>".$row['lugar']." ";
        $resultado .= "<td>".$row['fechaCreacion']."</td>";
        $resultado .= "</tr>";
      }

    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd); 

      $resultado .= "</tbody></table>";
      return $resultado;
  }

function getOpciones($id, $campo, $tabla) {
    $sql = "SELECT $id, $campo FROM $tabla";
    $result = sqlqry($sql);
    $option = "";

    while($row = mysqli_fetch_array($result)){
        $option = $option."<option value=".$row[0].">".ucfirst($row[1])."</option>";
    }

    return $option;
}

function agregarIncidente($idLugar, $idTipo) {
  //Le dices que esta va a ser tu consulta
    $sql = "CALL agregaIncidente('$idLugar', '$idTipo');";
    return sqlqry($sql);
}

function sqlqry($qry) {
    $con = conectar_bd();
    if(!$con){
        return false;
    }
//Mandas llamar el script del query
    $result = mysqli_query($con, $qry);
    desconectar_bd($con);
    return $result;
}

?>

