// Problema 1

function confirmacion(){
  let str1 = document.getElementById("contraseña").value;
  let str2 = document.getElementById("confirma").value;
  if(str1==str2){
    document.getElementById("demo").innerHTML = "Contraseña validada" ;
  }else{
    document.getElementById("demo").innerHTML = "Lo sentimos, las contraseñas no coinciden";
  }
}

document.getElementById("boton_confirma").onclick = confirmacion ;

//Problema 2
function papeleria(){
  let precio_libretas = 150;
  let precio_plumas = 50;
  let precio_pinceles = 60;
  let num_libretas = document.getElementById("libretas").value;
  let num_plumas = document.getElementById("plumas").value;
  let num_pinceles = document.getElementById("pinceles").value;
  let costo = 0;
  costo += num_plumas*precio_plumas;
  costo += num_libretas*precio_libretas;
  costo += num_pinceles*precio_pinceles;
  document.getElementById("costo_Total").innerHTML = "Tu compra consta de: <br>" + num_plumas + " plumas <br>" + num_pinceles + " pinceles<br>" + num_libretas + " libretas<br> El costo total de tu pedido es de: $" + costo;
}

document.getElementById("boton_tienda").onclick = papeleria;

//Problema 3

function test(){
  let puntosRojo = 0;
  let puntosMole = 0;
  let puntosVerde = 0;
  let puntosDulce = 0;
  if(document.getElementById("color").value == 1){
    puntosRojo++;
  }else  if(document.getElementById("color").value == 2){
    puntosMole++;
  }else if (document.getElementById("color").value == 3){
    puntosVerde++;
  }else{
    puntosDulce++;
  }

  if(document.getElementById("bebida").value == 1){
    puntosRojo++;
  }else  if(document.getElementById("bebida").value == 2){
    puntosMole++;
  }else if (document.getElementById("bebida").value == 3){
    puntosVerde++;
  }else{
    puntosDulce++;
  }

  if(document.getElementById("picor").value == 1){
    puntosRojo++;
  }else  if(document.getElementById("picor").value == 2){
    puntosMole++;
  }else if (document.getElementById("picor").value == 3){
    puntosVerde++;
  }else{
    puntosDulce++;
  }


  if(document.getElementById("palabra").value == 1){
    puntosRojo++;
  }else  if(document.getElementById("palabra").value == 2){
    puntosMole++;
  }else if (document.getElementById("palabra").value == 3){
    puntosVerde++;
  }else{
    puntosDulce++;
  }
  
  if(puntosRojo>=2){
    document.write("Eres Bombon!");
  }else if(puntosVerde>=2){
    document.write("Eres Bellota!");
  }else if(puntosMole>=2){
    document.write("Eres Burbuja!");
  }else{
    document.write("Eres Mojo Jojo");
  }

}

document.getElementById("listo").onclick = test;