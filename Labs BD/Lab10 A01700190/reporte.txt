CUESTIONARIO LAB 10

¿Que muestra la instrucción?
La información de la tabla por columnas.

¿Qué efecto tuvo esta acción?
Abrió una nueva ventana y ejecutó el script, creando las nuevas tablas.

¿Qué utilidad tiene esta manera de ejecutar los comandos de SQL?
No sé, a mí me gusta más la otra porque si me equivoco me dice exactamente en qué.

¿Qué relación tienen con las tablas de la base de datos? (checa su contenido)
Los archivos csv tiene la información que quiero cargar en las tablas. Sin embargo, con MySQL tuve un montón de problemas
para cargarlos, por lo que tuve que pasarlos a .txt

¿Qué relación tiene el contenido de este archivo (materiales.sql) con el formato en que se encuentran los datos en el archivo materiales.csv?
Te dice de dónde quieres sacar los datos, a dónde los quieres meter y cómo están separados los datos.

¿Qué sucedió?
Se muestra la tabla completa con los datos de Materiales.