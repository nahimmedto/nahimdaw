(select * from entregan where clave=1450)
union
(select * from entregan where clave=1300);
-- ¿Cuál sería una consulta que obtuviera el mismo resultado sin usar el operador Unión? Compruébalo.
Select * 
from entregan
where clave= 1300 or clave= 1450;

-- Nuevamente, "minus" es una palabra reservada que no está definida en SQL Server, 
-- define una consulta que regrese el mismo resultado.

Select * 
from entregan
where clave <> 1000;

Select *
from entregan, materiales;


SELECT DATE_FORMAT("01/01/00", "%d %M %Y");

Select DISTINCT Descripcion
from entregan as E, materiales as M
where e.clave = m.clave
AND Fecha between 01/01/00 and 31/12/00;


Select p.numero, denominacion , fecha, cantidad 
from proyectos as P, entregan as E
where p.numero = e.numero
Order by p.numero, fecha desc;

SELECT * FROM materiales where Descripcion LIKE 'Si%';
SELECT * FROM materiales where Descripcion LIKE 'Si';

DECLARE foo varchar(40);
DECLARE @bar varchar(40);
SET @foo = '¿Que resultado';
SET @bar = ' ¿¿¿??? '
SET @foo += ' obtienes?';
PRINT @foo + @bar;
