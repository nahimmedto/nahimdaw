<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php
      $arreglo1 = array(7,8,2,9,1);
      $arreglo2 = array(14,23,67,91,34,56);

      function promedio($arr){
        $suma = 0;
        foreach($arr as $numero){
          $suma += $numero;
        }
        $cuantosNum = count($arr);
        $promedio = $suma / $cuantosNum;
        return $promedio;
      }
      $promedio1 = promedio($arreglo1);
      $promedio2 = promedio($arreglo2);
      echo "Prueba 1 de promedio: $promedio1<br>";
      echo "Prueba 2 de promedio: $promedio2<br>";

      function mediana($arr){
        sort($arr);
        $cuantosNum = count($arr);
        $mid = floor(($cuantosNum - 1) / 2);
        if($cuantosNum % 2 == 0){
          $low = $arr[$mid];
          $high = $arr[$mid + 1];
          $median = (($low + $high) / 2 );
        } else {
          $median = $arr[$mid];
        }
        return $median;
      }

      $mediana1 = mediana($arreglo1);
      $mediana2 = mediana($arreglo2);
      echo "Prueba 1 de mediana: $mediana1<br>";
      echo "Prueba 2 de mediana: $mediana2<br>";

      function ejercicio3($arr){
        sort($arr);
        echo "<h3>De menor a mayor: </h3>";
        echo "<ul>";
        foreach($arr as $numero){
          echo "<li>$numero</li>" ;
        }
        echo "</ul>";
        arsort($arr);
        echo "<h3>De mayor a menor: </h3>";
        echo "<ul>";
        foreach($arr as $numero){
          echo "<li>$numero</li>" ;
        }
        echo "</ul>";
      }

      $pruebaejercicio3_1 = ejercicio3($arreglo1);
      echo "$pruebaejercicio3_1";
      $pruebaejercicio3_2 = ejercicio3($arreglo2);
      echo "$pruebaejercicio3_2";

    function tabla($n){
      $data = array();
		  for($i = 1; $i <= $n; $i++)
		  {
        array_push($data, array("n"=>$i, "n²"=>pow($i, 2), "n³"=>pow($i,3)));
      }
		return $data;
    }

    $datostabla1 = tabla(5);
    $datostabla2 = tabla(2);

	?>

  <h2> Preguntas </h2>
  <p><strong> ¿Qué hace la función phpinfo()?</strong></p>
  <p>Sirve para revisar las configuraciones, así como revisar las variables predefinidas en un sistema. Lo interesante es que puede usarse para debuggear, te enseña las configuraciones del dispositivo desde el cuál se envía la página.</p>
  <p><strong>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</strong></p>
  <p>
  <p><strong>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor?</strong></p>
  <p>Porque se ejecuta mandando una solicitud HTTP al servidor, y éste le regresa el html para que el cliente (quien hace la solicitud) pueda interpretarlo.</p>
  </body>
</html>